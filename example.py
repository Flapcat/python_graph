"""
Example script for using the graph class
Author:    Jed Hollom
Date:      September 2016
Email: Jedhollom@gmail.com
"""

from graph import Graph

# Structure
# A -> B
# A -> C
# C -> D
# C -> E
# E -> F
nodes = ['a', 'b', 'c', 'd', 'e', 'f']


# Build graph object
new_graph = Graph()

# Add nodes to graph
new_graph.add_nodes(nodes)

# Check nodes
print new_graph.get_nodes()
print ''

# Add a link to graph
new_graph.add_links(['a', 'b'])

# Check node 'a' links
print new_graph.get_links('a')
print ''

# Add rest of links to graph
new_graph.add_links([['a', 'c'], ['c', 'd'], ['c', 'e'], ['e', 'f']])

# Check all links are present
print new_graph.get_graph()
print ''

# Check edge nodes
print new_graph.get_edges()
print ''

# Check connection between 'b' and 'f'
print new_graph.check_connection('b', 'f')
print ''

# Remove link between 'a' and 'c'
new_graph.remove_links(['c', 'e'])

# Check node 'a' links
print new_graph.get_links('c')
print ''

# Check connection between 'c' and 'e'
print new_graph.check_connection('c', 'e')
print ''

# Remove node 'b'
new_graph.remove_nodes('b')

# Check all links are present
print new_graph.get_graph()
print ''
